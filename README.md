# Technical assignment
This is a PHP assignment enabling you to show off your development skills and impress us.

## Description
Fetch data from our API, model it into your own entities and persist them to db.
Add a job to synchronize this data with the API.
Then output a list of entities.

## APIs
Use the following APIs: 
* https://rdbauth.staging.sidekickit.nl/token
* https://rdbapi.staging.sidekickit.nl

Documentation: https://rdbapi.staging.sidekickit.nl/docs

## Assignment
We'd like to assess what you can do in 2-4 hours:

* Start a fresh project using either Laminas or Symfony framework
* Authenticate (oAuth2) your application to the auth endpoint with the client_id, client_secret, username and password you received from us
  * Supply the 'Authorization' header with a 'Bearer' token 
* Consume the API endpoints required for the 'Guideline', 'GuidelineNavigation', 'Module', 'ModuleReference' and 'ModuleHyperlink' entities 
    * You may skip irrelevant 'fields' to save time, especially the Module entity has many fields not relevant for this assignment  
* Model your own Doctrine entities after the API entities
* Import the 25 newest Guidelines and their related entities (only those mentioned earlier) to your own database
* The import routine should be executable as a (cron) job
* Output a nested list (i.e. ul > li) of each Guideline with the titles of their child GuidelineNavigation objects sorted by order
    * How you output (i.e. console or browser) is not important
* Apply best practices and design patterns where you can

## Bonus points
* Use the GraphQL endpoint to only fetch what you require
* Divide your solution into separate modules or bundles
* Write a docker-compose file and use secrets for sensitive keys
* Write tests (unit/integration)
* Both APIs suffer from some issues and flaws, name them and suggest improvements

## Questions
Don't hesitate to contact us! We're here to assist.

## Done
Please send us a GitLab/GitHub link to your solution or send us an archive.
